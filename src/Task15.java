import java.util.ArrayList;

public class Task15 {
    public static void main(String[] args) {
        ArrayList<Person> contactList = new ArrayList<>();
        contactList.add(new Person("John", "TheBoy"));
        contactList.add(new Person("Dewald", "TheBrave"));
        contactList.add(new Person("Craig", "TheWizzard"));
        contactList.add(new Person("Nicholas", "TheBard"));
        contactList.add(new Person("Michael", "TheWise"));

        int matchesFound = 0;
        if(args.length == 0){
            System.out.println("Please enter a name as an argument");
        }else{
            for(Person contact : contactList){
                if(contact.getName().contains(args[0].toLowerCase())){
                    System.out.println(contact.getName() + " " + contact.getLastName());
                    matchesFound++;
                }
            }
            if(matchesFound == 0){
                System.out.println("No matching contact names were found");
            }
        }

    }
}
