public class Person {
    private String name;
    private String lastName;
    private int number;

    public Person() {
        this.name = "unnamed";
        this.lastName = "unnamed";
        this.number = 0;
    }

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public Person(String name, String lastName, int number) {
        this.name = name;
        this.lastName = lastName;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getNumber() {
        return number;
    }
}
